# estarossa

> Estarossa est une petite application pouvant vous aider à gérer votre expérience pour gérer des guildes, il permet d'envoyer des messages à plusieurs serveurs discord via les webhooks
Vidéo de presentation : https://www.youtube.com/watch?v=IPIksdTquKY&feature=youtu.be

#### Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:9080
npm run dev

# build electron application for production
npm run build

# run unit & end-to-end tests
npm test


# lint all JS/Vue component files in `src/`
npm run lint

```