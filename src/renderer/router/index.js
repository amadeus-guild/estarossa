import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'welcome-view',
      component: require('@/components/WelcomeView').default
    },
    {
      path: '/message',
      name: 'message',
      component: require('@/components/MessageAlly').default
    },
    {
      path: '/ally',
      name: 'Ally',
      component: require('@/components/CreateAlly').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
