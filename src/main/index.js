'use strict'

import { app, BrowserWindow } from 'electron'
import fs from 'fs-extra'
/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let mainWindow
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`
function init () {
  fs.ensureDir(app.getPath('userData') + '/heart/', (err) => {
    if (err) {

    } else {
      var files = ['ally.json']
      files.forEach(val => {
        fs.writeFile(app.getPath('userData') + '/heart/' + val, '{}', function (err) {
          if (err) {
            console.log(err)
          }
          console.log('The file was saved!')
        })
      })
    }
  })
}
function createWindow () {
  /**
   * Initial window options
   */
  fs.pathExists(app.getPath('userData') + '/heart/').then(exists => {
    if (!exists) {
      init()
      console.log('Doesn\'t exist')
    } else {
      console.log('exist')
    }
  })

  mainWindow = new BrowserWindow({
    height: 563,
    useContentSize: true,
    width: 1000
  })

  mainWindow.loadURL(winURL)

  mainWindow.on('closed', () => {
    mainWindow = null
  })
}
app.setPath('userData', app.getPath('appData') + '/Estarossa')
app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
